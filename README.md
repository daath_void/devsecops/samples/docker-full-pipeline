# devsecops-containers



## Pré-requisitos

Acesso ao host com usuario comum via ssh autenticado por chave sem senha.
Usuario comum no host com direito a sudo sem senha

Nota: Para melhorar a segurança leia sobre ansible vault: https://docs.ansible.com/ansible/latest/user_guide/vault.html


## Uso do ansible

```
vim inventory

[all:vars]
ansible_user=suporte
ansible_become=true

[devsecops]
172.19.9.100
```

Depois execute o ansible dentro do diretorio para ajustar os requisitos de host:

```
ansible-playbook main.yml 
```



## Uso do docker-compose

Edite o .env e altere as variaveis de acordo com o seu ambiente

```
vim .env

DEFAULT_DNS=8.8.8.8
NETWORK=172.19.1
NETWORK_RANGE=${NETWORK}.0/24
NEXUS_IP=${NETWORK}.11
POSTGRES_IP=${NETWORK}.12
POSTGRES_ROOTPW=password
POSTGRES_INIT=./sonarqube
JENKINS_IP=${NETWORK}.14
JENKINS_HOME=/srv/jenkins
GITLAB_IP=${NETWORK}.10
GITLAB_HOME=/srv/gitlab
GITLAB_HOSTNAME=git.example.com
GITLAB_EXTERNAL_URL=https://git.example.com
SONAR_IP=${NETWORK}.13
SONAR_HOME=/srv/sonarqube
SONAR_SQLPW=password
NGINX_IP=${NETWORK}.200
NGINX_HOME=/srv/nginx
MODSEC_PARANOIA=1
DEFAULT_LDAP_HOST=ldap.example.com
DEFAULT_LDAP_URL=ldap://ldap.example.com:389
DEFAULT_LDAP_BINDDN=cn=system,ou=System Accounts,dc=example,dc=com
DEFAULT_LDAP_BINDPASSWORD=password
DEFAULT_LDAP_AUTHENTICATION=simple
DEFAULT_LDAP_REALM=example.com
DEFAULT_LDAP_USER_BASEDN=OU=People,DC=example,DC=com
DEFAULT_LDAP_GROUP_BASEDN=ou=Groups,DC=hpp,DC=org,DC=br
DEFAULT_LDAP_ADMIN_GROUP=IT-OPS

```

O Compose pode ser executado manualmente dentro do diretório:

```
docker-compose up -d
```

Se quiser verificar os logs de um container:

```
docker-compose logs <container>
```

Ex.:

```
docker-compose logs gitlab
```

Para carregar um container:

```
docker-compose up -d <container>
```

Para reiniciar um container:

```
docker-compose restart <container>
```

Para derrubar todos os containers:

```
docker-compose down
```

Para testar configuração do compose:

```
docker-compose -f docker-compose.yml config
```



